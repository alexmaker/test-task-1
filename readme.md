## Getting started
Execute commands below.
```bash
composer install
# Run tests with PhpUnit
./vendor/bin/phpunit --bootstrap vendor/autoload.php tests
```

Run web server
```bash
php -S localhost:8000
```

## Code review
Check it out here https://docs.google.com/document/d/1FRC-V2PdRz2M1iJ3pvNhgQMaLEYSKL4zFDGxwToc5NQ/edit?usp=sharing.

## What's use
1. PHP 7.3
2. Composer
3. psr/simple-cache
4. psr/log
5. Twig 
6. Nasa Rest Service
7. PHPUnit

