<?php

namespace Skyeng\Testwork\Tests;

use PHPUnit\Framework\TestCase;
use Skyeng\Testwork\FileLogger;
use Psr\Log\LogLevel;


final class FileLoggerTest extends TestCase
{
    private $logger;
    private $dir = "./logs";
    private $filename = "phpunit";

    public function setUp() :void
    {
        $fn = $this->dir . "/" . $this->filename;
        if (file_exists($fn)) {
            unlink($fn);
        }

        try {
            $this->logger = new FileLogger($this->dir, LogLevel::DEBUG);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
        $this->logger->setCustomName($this->filename);
    }

    /**
     * @testCase Logger implements PSR-3 Psr\Log\LoggerInterface
     */
    public function testLoggerImplementsPRS3Interface()
    {
        $this->assertInstanceOf(\Psr\Log\LoggerInterface::class, $this->logger);
    }

    /**
     * @testCase     setLogLevel sets the correct log level.
     * @throws       \Exception
     */
    public function testSetLogLevel()
    {
        $this->logger->setLogLevel(LogLevel::ERROR);
        $property = new \ReflectionProperty(FileLogger::class, 'level');
        $property->setAccessible(true);
        // When
        $code = $property->getValue($this->logger);
        // Then
        $this->assertEquals('error', $code);
    }

    /**
     * @testCase     Trying to log all types errors
     * @throws       \Exception
     */
    public function testLog()
    {
        $this->logger->setLogLevel(LogLevel::DEBUG);
        $levels = $this->logger->getLogLevels();
        $trait = ["phpunit" => true, "exception" => new \Exception("phpunit test")];

        foreach($levels as $value) {
            $success = $this->logger->$value("Trying to log from level {$value}", $trait);
            $this->assertNotFalse($success);
        }
    }

}
