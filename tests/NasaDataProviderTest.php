<?php

namespace Skyeng\Testwork\Tests;

use PHPUnit\Framework\TestCase;
use Skyeng\Testwork\NasaDataProvider;
use Skyeng\Testwork\FileLogger;


final class NasaDataProviderTest extends TestCase
{

   /**
     * @testCase Test some data from Api
     */
    public function testGetData()
    {
        $provider = new NasaDataProvider('https://api.nasa.gov');
        $provider->setEndpoint('/planetary/apod');
        $provider->setToken('IHIZKHsadYCjTVQbH0xcODKf2RNhUYWMUjTZcS0E');
        $data = $provider->request([]);
        $this->assertIsObject($data);
        $this->assertObjectHasAttribute("date", $data);
        $this->assertObjectHasAttribute("explanation", $data);
        $this->assertObjectHasAttribute("url", $data);
    }
}