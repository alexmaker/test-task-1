<?php

namespace Skyeng\Testwork\Tests;

use PHPUnit\Framework\TestCase;
use Skyeng\Testwork\FileCache;

final class FileCacheTest extends TestCase
{
    /**
     * @testCase Test cache
     */
    public function testCache()
    {
        $cache = new FileCache("./cache", 600);
        $test = new \stdClass();
        $test->check = true;
        $key = md5('test');
        $cache->set($key, $test);

        $get = $cache->get($key);
        $this->assertObjectHasAttribute("check", $get);
        $this->assertTrue($get->check);
    }
}