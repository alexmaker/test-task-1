<?php
require_once './vendor/autoload.php';

use Skyeng\Testwork\NasaDataProvider;
use Skyeng\Testwork\FileLogger;
use Psr\Log\LogLevel;
use Skyeng\Testwork\FileCache;

$loader = new \Twig\Loader\FilesystemLoader('./templates');
$twig = new \Twig\Environment($loader, []);

$logger = new FileLogger('./logs', LogLevel::WARNING);
$cache = new FileCache('./cache', 3600, 0777);

$provider = new NasaDataProvider('https://api.nasa.gov');
$provider->setEndpoint('/planetary/apod');
$provider->setToken('IHIZKHsadYCjTVQbH0xcODKf2RNhUYWMUjTZcS0E');
$provider->setLogger($logger);
$provider->setCache($cache);

try {
    $data = $provider->request([]);
    echo $twig->render('index.html', array('title' => 'Skyeng testwork', 'data' => $data));
} catch (Exception $e) {
    echo "Error: " . $e->getMessage();
}
