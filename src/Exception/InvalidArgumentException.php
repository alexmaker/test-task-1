<?php

namespace Skyeng\Testwork\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements \Psr\SimpleCache\InvalidArgumentException
{
}