<?php


namespace Skyeng\Testwork;

use Psr\Log\AbstractLogger;
use Psr\Log\LoggerInterface;
use Exception;
use ReflectionClass;


class FileLogger extends AbstractLogger implements LoggerInterface
{
    const TAB = "\t";

    private $dir;
    private $level;
    private $custom_name;

    /**
     * Constructor
     *
     * @param string $dir
     * @param string $level
     *
     * @throws \Exception
     */
    public function __construct(string $dir, string $level)
    {
        $this->setDir($dir);
        $this->setLogLevel($level);
    }

    /**
     * Set the lowest log level to log.
     *
     * @param string $level
     * @throws \Exception
     */
    public function setLogLevel(string $level)
    {
        $levels = $this->getLogLevels();
        if (!in_array($level, $levels)) {
            throw new Exception("Log level {$level} is not a valid log level. Must be one of (" . implode((array)', ', array_keys($levels) . ')'));
        }

        $this->level = $level;
    }

    /**
     * Setter method for log directory
     *
     * @param string $dir
     * @throws \Exception
     */

    public function setDir(string $dir)
    {
        $this->checkDir($dir);
        $this->dir = rtrim($dir, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
    }

    /**
     * Setter method for custom name log file
     *
     * @param string $fname
     */
    public function setCustomName(string $fname)
    {
        $this->custom_name = $fname;
    }

    /**
     * Returns all log levels
     *
     * @return array
     * @throws \ReflectionException
     */
    public function getLogLevels() :array
    {
        $oClass = new ReflectionClass("Psr\Log\LogLevel");
        return $oClass->getConstants();
    }

    /**
     * Check the log directory
     *
     * @param string $dir
     * @throws \Exception
     */

    private function checkDir(string $dir)
    {
        if (!is_dir($dir)) {
            throw new Exception('Directory "' . $dir . '" is not found or is not directory.');
        }
    }

    /**
     * Get the filename custom or date formating
     *
     */
    private function getFileName() :string
    {
        $file = $this->custom_name ?:date("Y-m-d");
        return $file . ".log";
    }


    /**
     * Basic function to log
     *
     * @param string $level
     * @param string $message
     * @param array $context
     * @return bool
     * @throws Exception
     */
    public function log($level, $message, array $context = []) :bool
    {
        $pid = getmypid();
        list($exception, $context) = $this->handleException($context);
        $context = $context ? json_encode($context, \JSON_UNESCAPED_SLASHES) : '{}';
        $context = $context ?: '{}'; // Fail-safe incase json_encode fails.
        $line = $this->formatLogLine($level, $pid, $message, $context, $exception);
        $fn = $this->dir . $this->getFileName();

        try {
            $fh = fopen($fn, 'a');
            fwrite($fh, $line);
            fclose($fh);
        } catch (\Throwable $e) {
            throw new \RuntimeException("Could not open log file {$fn} for writing!", 0, $e);
        }

        return true;
    }

    /**
     * Format the log line.
     * YYYY-mm-dd HH:ii:ss.uuuuuu  [loglevel]  [channel]  [pid:##]  Log message content  {"Optional":"JSON Contextual Support Data"}  {"Optional":"Exception Data"}
     *
     * @param string $level
     * @param int $pid
     * @param string $message
     * @param string $context
     * @param string $exception
     *
     * @return string
     * @throws Exception
     */
    private function formatLogLine(string $level, int $pid, string $message, string $context, string $exception): string
    {
        return
            $this->getTime()                              . self::TAB .
            "[$level]"                                    . self::TAB .
            "[pid:$pid]"                                  . self::TAB .
            str_replace(\PHP_EOL, '   ', trim($message))  . self::TAB .
            str_replace(\PHP_EOL, '   ', $context)           . self::TAB .
            str_replace(\PHP_EOL, '   ', $exception) . \PHP_EOL;
    }

    /**
     * Get current date time.
     * Format: YYYY-mm-dd HH:ii:ss.uuuuuu
     * Microsecond precision for PHP 7.1 and greater
     *
     * @return string Date time
     * @throws Exception
     */
    private function getTime(): string
    {
        return (new \DateTimeImmutable('now'))->format('Y-m-d H:i:s.u');
    }

    /**
     * Handle an exception in the data context array.
     * If an exception is included in the data context array, extract it.
     *
     * @param array|null $context
     * @return array  [exception, data (without exception)]
     */
    private function handleException(array $context = null): array
    {
        if (isset($context['exception']) && $context['exception'] instanceof \Throwable) {
            $exception      = $context['exception'];
            $exception_data = $this->buildExceptionData($exception);
            unset($context['exception']);
        } else {
            $exception_data = '{}';
        }
        return [$exception_data, $context];
    }

    /**
     * Build the exception log data.
     *
     * @param  \Throwable $e
     *
     * @return string JSON {message, code, file, line, trace}
     */
    private function buildExceptionData(\Throwable $e): string
    {
        $exceptionData = json_encode(
            [
                'message' => $e->getMessage(),
                'code'    => $e->getCode(),
                'file'    => $e->getFile(),
                'line'    => $e->getLine(),
                'trace'   => $e->getTrace()
            ],
            \JSON_UNESCAPED_SLASHES
        );
        // Fail-safe in case json_encode failed
        return $exceptionData ?: '{"message":"' . $e->getMessage() . '"}';
    }
}