<?php


namespace Skyeng\Testwork;

use Psr\SimpleCache\CacheInterface;
use Skyeng\Testwork\Exception\InvalidArgumentException;
use DateInterval;
use Traversable;
use RecursiveDirectoryIterator;
use FilesystemIterator;
use RecursiveIteratorIterator;
use Generator;
use function gettype;
use function is_int;
use function file_exists;

class FileCache implements CacheInterface
{
    /**
     * @var string control characters for keys, reserved by PSR-16
     */
    const PSR16_KEYS = '/\{|\}|\(|\)|\/|\\\\|\@|\:/u';

    private $dir;
    private $ttl;
    private $mode;

    public function __construct(string $dir, int $ttl, int $mode = 0777)
    {
        $this->setDir($dir);
        $this->ttl = $ttl;
        $this->mode = $mode;
    }

    /**
     * Setter for the cache dir
     *
     * @param string $dir
     * @throws InvalidArgumentException
     */
    public function setDir(string $dir)
    {
        $this->checkDir($dir);
        $this->dir = $dir;
    }

    /**
     * Makes the dir if needed and check that dir
     *
     * @param string $path
     * @return bool
     * @throws InvalidArgumentException
     */
    private function checkDir(string $path) :bool
    {
        $dir = dirname($path);

        if (!file_exists($dir)) {
            if(!mkdir($dir, $this->mode, true)) {
                throw new InvalidArgumentException('Can\'t create  "' . $dir);
            }
        }

        if (!is_dir($dir)) {
            throw new InvalidArgumentException('Directory "' . $dir . '" is not found or is not directory.');
        }

        return true;
    }


    /**
     * Gets the key of cache
     *
     * @param string $str
     * @return string md5 cash of request
     *
     */
    public function getKey(string $str) :string
    {
        return md5($str);
    }

    /**
     * @return Generator|string[]
     */
    public function listPaths()
    {
        $iterator = new RecursiveDirectoryIterator(
            $this->dir,
            FilesystemIterator::CURRENT_AS_PATHNAME | FilesystemIterator::SKIP_DOTS
        );
        $iterator = new RecursiveIteratorIterator($iterator);
        foreach ($iterator as $path) {
            if (is_dir($path)) {
                continue; // ignore directories
            }
            yield $path;
        }
    }

    /**
     * Clean up expired cache-files.
     *
     * This method is outside the scope of the PSR-16 cache concept, and is specific to
     * this implementation, being a file-cache.
     *
     * In scenarios with dynamic keys (such as Session IDs) you should call this method
     * periodically - for example from a scheduled daily cron-job.
     *
     * @return void
     */
    public function cleanExpired()
    {
        $now = time();
        $paths = $this->listPaths();
        foreach ($paths as $path) {
            if ($now > filemtime($path)) {
                @unlink($path);
            }
        }
    }


    /**
     * For a given cache key, obtain the absolute file path
     *
     * @param string $key
     *
     * @return string absolute path to cache-file
     *
     * @throws InvalidArgumentException if the specified key contains a character reserved by PSR-16
     */
    private function getPath($key)
    {
        $this->validateKey($key);

        return $this->dir
            . DIRECTORY_SEPARATOR
            . strtolower($key[0])
            . DIRECTORY_SEPARATOR
            . strtolower($key[0] . $key[1])
            . DIRECTORY_SEPARATOR
            . $key;
    }

    /**
     * @param string $key
     *
     * @throws InvalidArgumentException
     */
    private function validateKey($key)
    {
        if (! is_string($key)) {
            $type = is_object($key) ? get_class($key) : gettype($key);
            throw new InvalidArgumentException("invalid key type: {$type} given");
        }
        if ($key === "") {
            throw new InvalidArgumentException("invalid key: empty string given");
        }
        if ($key === null) {
            throw new InvalidArgumentException("invalid key: null given");
        }
        if (preg_match(self::PSR16_KEYS, $key, $match) === 1) {
            throw new InvalidArgumentException("invalid character in key: {$match[0]}");
        }
    }

    /**
     * Fetches a value from the cache.
     *
     * @param string $key The unique key of this item in the cache.
     * @param mixed $default Default value to return if the key does not exist.
     *
     * @return mixed The value of the item from the cache, or $default in case of cache miss.
     *
     * @throws \Psr\SimpleCache\InvalidArgumentException
     *   MUST be thrown if the $key string is not a legal value.
     */
    public function get($key, $default = null)
    {
        $this->validateKey($key);
        $path = $this->getPath($key);
        $expires_at = @filemtime($path);

        if ($expires_at === false) {
            return $default; // file not found
        }
        if (time() >= $expires_at) {
            @unlink($path); // file expired
            return $default;
        }
        $data = @file_get_contents($path);
        if ($data === false) {
            return $default; // race condition: file not found
        }
        if ($data === 'b:0;') {
            return false; // because we can't otherwise distinguish a FALSE return-value from unserialize()
        }
        $value = @unserialize($data);
        if ($value === false) {
            return $default; // unserialize() failed
        }
        return $value;
    }

    /**
     * Persists data in the cache, uniquely referenced by a key with an optional expiration TTL time.
     *
     * @param string $key The key of the item to store.
     * @param mixed $value The value of the item to store, must be serializable.
     * @param null|int|\DateInterval $ttl Optional. The TTL value of this item. If no value is sent and
     *                                      the driver supports TTL then the library may set a default value
     *                                      for it or let the driver take care of that.
     *
     * @return bool True on success and false on failure.
     *
     * @throws \Psr\SimpleCache\InvalidArgumentException
     *   MUST be thrown if the $key string is not a legal value.
     */
    public function set($key, $value, $ttl = null) :bool
    {
        $this->validateKey($key);
        $path = $this->getPath($key);
        $this->checkDir($path);

        if (is_int($ttl)) {
            $expires_at = time() + $ttl;
        } elseif ($ttl instanceof DateInterval) {
            $expires_at = date_create_from_format("U", time())->add($ttl)->getTimestamp();
        } elseif ($ttl === null) {
            $expires_at = time() + $this->ttl;
        } else {
            throw new InvalidArgumentException("invalid TTL: " . var_export($ttl, true));
        }

        if(!@file_put_contents($path, serialize($value))) {
            throw new InvalidArgumentException("Can't write the cache " . $path);
        }

        if (!@touch($path, $expires_at)) {
            throw new InvalidArgumentException("Can't set expire time of the " . $path);
        }

        return true;
    }

    /**
     * Delete an item from the cache by its unique key.
     *
     * @param string $key The unique cache key of the item to delete.
     *
     * @return bool True if the item was successfully removed. False if there was an error.
     *
     * @throws \Psr\SimpleCache\InvalidArgumentException
     *   MUST be thrown if the $key string is not a legal value.
     */
    public function delete($key)
    {
        $this->validateKey($key);
        $path = $this->getPath($key);
        return !file_exists($path) || @unlink($path);
    }

    /**
     * Wipes clean the entire cache's keys.
     *
     * @return bool True on success and false on failure.
     */
    public function clear()
    {
        $success = true;
        $paths = $this->listPaths();
        foreach ($paths as $path) {
            if (! unlink($path)) {
                $success = false;
            }
        }
        return $success;
    }

    /**
     * Obtains multiple cache items by their unique keys.
     *
     * @param iterable $keys A list of keys that can obtained in a single operation.
     * @param mixed $default Default value to return for keys that do not exist.
     *
     * @return iterable A list of key => value pairs. Cache keys that do not exist or are stale will have $default as value.
     *
     * @throws \Psr\SimpleCache\InvalidArgumentException
     *   MUST be thrown if $keys is neither an array nor a Traversable,
     *   or if any of the $keys are not a legal value.
     */
    public function getMultiple($keys, $default = null)
    {
        if (!is_array($keys) && ! $keys instanceof Traversable) {
            throw new InvalidArgumentException("keys must be either of type array or Traversable");
        }
        $values = [];
        foreach ($keys as $key) {
            $values[$key] = $this->get($key) ?: $default;
        }
        return $values;
    }

    /**
     * Persists a set of key => value pairs in the cache, with an optional TTL.
     *
     * @param iterable $values A list of key => value pairs for a multiple-set operation.
     * @param null|int|\DateInterval $ttl Optional. The TTL value of this item. If no value is sent and
     *                                       the driver supports TTL then the library may set a default value
     *                                       for it or let the driver take care of that.
     *
     * @return bool True on success and false on failure.
     *
     * @throws \Psr\SimpleCache\InvalidArgumentException
     *   MUST be thrown if $values is neither an array nor a Traversable,
     *   or if any of the $values are not a legal value.
     */
    public function setMultiple($values, $ttl = null)
    {
        if (! is_array($values) && ! $values instanceof Traversable) {
            throw new InvalidArgumentException("keys must be either of type array or Traversable");
        }
        $ok = true;
        foreach ($values as $key => $value) {
            if (is_int($key)) {
                $key = (string) $key;
            }
            $ok = $this->set($key, $value, $ttl) && $ok;
        }
        return $ok;
    }

    /**
     * Deletes multiple cache items in a single operation.
     *
     * @param iterable $keys A list of string-based keys to be deleted.
     *
     * @return bool True if the items were successfully removed. False if there was an error.
     *
     * @throws \Psr\SimpleCache\InvalidArgumentException
     *   MUST be thrown if $keys is neither an array nor a Traversable,
     *   or if any of the $keys are not a legal value.
     */
    public function deleteMultiple($keys)
    {
        if (!is_array($keys) && ! $keys instanceof Traversable) {
            throw new InvalidArgumentException("keys must be either of type array or Traversable");
        }
        $ok = true;
        foreach ($keys as $key) {
            $ok = $ok && $this->delete($key);
        }
        return $ok;
    }

    /**
     * Determines whether an item is present in the cache.
     *
     * NOTE: It is recommended that has() is only to be used for cache warming type purposes
     * and not to be used within your live applications operations for get/set, as this method
     * is subject to a race condition where your has() will return true and immediately after,
     * another script can remove it making the state of your app out of date.
     *
     * @param string $key The cache item key.
     *
     * @return bool
     *
     * @throws \Psr\SimpleCache\InvalidArgumentException
     *   MUST be thrown if the $key string is not a legal value.
     */
    public function has($key)
    {
        return $this->get($key, $this) !== $this;
    }
}