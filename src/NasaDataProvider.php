<?php


namespace Skyeng\Testwork;

use mysql_xdevapi\Executable;
use phpDocumentor\Reflection\Types\Object_;
use Psr\Log\LoggerInterface;
use Psr\Log\LoggerAwareInterface;
use Exception;


class NasaDataProvider implements LoggerAwareInterface
{
    private $endpoint;
    private $token;
    private $server;

    private $logger;
    private $cache;

    /**
     * @param string $token The token of the Nasa Api
     */
    public function __construct(string $server)
    {
        $this->server = $server;
    }

    /**
     * @param LoggerInterface $logger The logger object
     */
    public function setLogger(LoggerInterface $logger) {
        $this->logger = $logger;
    }

    /**
     * @param FileLogger $cache The cache object
     */
    public function setCache($cache) {
        $this->cache = $cache;
    }

    /**
     * @param string $server The url of server
     */
    public function setToken(string $token) {
        $this->token = $token;
    }

    /**
     * @param string $endpoint The url of endpoint
     */
    public function setEndpoint(string $endpoint) {
        $this->endpoint = $endpoint;
    }


    public function getUrl(array $query) : string
    {
        //todo: check the schema in use
        $url = $this->server . $this->endpoint .
            "?api_key=" . $this->token;
        if(count($query))
            $url .= "&" . http_build_query($query);

        return $url;
    }

    /**
     * @param  array $params The query to api
     * @return \stdClass  json answer from server
     * @throws Exception
     */
    public function request(array $params) :?\stdClass
    {
        $url = $this->getUrl($params);

        try {
            if($this->cache) {
                $data = $this->cache->get($this->cache->getKey($url));
                if($data) {
                    return $data;
                }
            }

            $data = json_decode($this->get($url));
            if($this->cache) {
                $this->cache->set($this->cache->getKey($url), $data);
            }

            return $data;
        } catch (Exception $e) {
            if($this->logger) {
                $this->logger->error($e->getMessage(), ["url" => $url, "exception" => $e]);
            }

            throw $e;
        }
    }

    /**
     * @param  string $url
     * @return string The json answer from server
     * @throws Exception
     */
    private function get(string $url) :string
    {
        // todo: use Curl to get HTTP Statuses and throw exceptions better
        $data = @file_get_contents($url);
        if(!$data)
            throw new Exception("Can't load {$this->server}{$this->endpoint} endpoint");

        return $data;
    }
}